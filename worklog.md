**WEEK34**


- **_Summary of Activities;_**

      -I was absent but watched the Nordcad Beginner Course later on.
      -I chose to work with the RP2040 development board but later on chose to work with an IOT HAT because I wanted to find an effective way of watering my plants without killing them due to forgetfulness or Vacations.
      


- **_Lessons Learnt;_**
  
        -How HATs work and their different types 
  




- **_Resources Used;_**

       -Materials provided by NISI
       -I also watched this youtube video for inspiraton;  https://www.youtube.com/watch?v=pg-g3hVwOHA


**WEEK35**


- **_Summary of Activities;_**

      - I set up a gitlab Project as instructed by NISI. 
      - Called Nordcad to help out with my license.  
      - Continued with the courses from where I left at.
      - Got started with drawing my schematics.
      -Thought of making another PCB for a project I was working on.



- **_Lessons Learnt;_**
   




- **_Resources Used;_**
  


**WEEK36**

- **_Summary of Activities;_**

      -Did research on SMT Packages and created a spreadsheet of my findings as instructed by NISI.
      -Created a list of components I will use on my project(yet to be uploaded on gitlab)
      -Fought with Orcad as per usual and got some progress.


I decided to restart my working all over again and chose to work on a solar tracker PCB.

